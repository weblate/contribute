# front

The Front-end app that powers https://contribateliers.frama.io/contribute, forked from https://contribute.funkwhale.audio/

## Edit tasks/guides

The wordings, steps and screenshots are all described in
https://framagit.org/contribateliers/contribute/blob/master/src/mixins/TaskMixin.js

### Adding a task

Tasks and guides are described in javascript to allow for clean separation with the templates
and internationalization.

Don't worry, you can still write one even if you're not a developper, we'll show you how.

In `Contribute`, tasks represent things you can do to help the project. Tasks
are described in `src/data.js`.

A task is looks like that:

```javascript
{
  // the name of the task
  name: "Donate to the project",
  // an icon to represent the task, pick one from https://forkawesome.github.io/Fork-Awesome/icons/
  icon: "eur",
  // how much minutes does it take to complete the task
  duration: 30,
  // A longer decscription or summary for the task
  summary: "Donations help us keep the project running!",
}
```

Adding this task to the list of available task (whch is displayed on the homepage)
is made by appending the previous object to the task array, in `src/data.js`, like this:

```javascript
export function getTasks (v) {
  return [
    // existing tasks here
    {...},
    {...},
    {...},
    {...},
    {
      name: "Donate to the project",
      icon: "eur",
      duration: 30,
      summary: "Donations help us keep the project running!",
    }
  ]
}
```
Once you've done that, your new task should appear on the homepage.

### Adding a guide

However, by itself, your task is not especially useful for readers. To ensure
people contribute, you have to describe how people can complete a given task.
This is done by adding a few attributes to the task object we created earlier:

```javascript
{
  name: "Donate to the project",
  icon: "eur",
  duration: 5,
  summary: "Donations help us keep the project running!",
  // following attributes are needed to have a guide for the task

  // the slug will be included in the URL for the guide, like that:
  // https://contribute.funkwhale.audio/guides/:slug
  // It's important to keep the same slug once the guide is deployed
  // to not break existing links
  slug: 'donate-to-the-project',
  // Steps are the core of a guide. They are displayed in order in the guide,
  // used to build the table of contents, and can contain link and images
  steps: [
    {
      // Title of the step (displayed in the step box and the ToC)
      title: 'Create an Open Collective account',
      // long form instructions of the step
      content: "We use Open Collective as our donation platform, you'll need an account there.",
      // some steps are not required (for instance here, the reader may already have an account)
      // so you can mark a step as optional
      optional: true,
      // use this attribute to add one or more links under the step
      links: [
        {
          text: 'Visit Open Collective',
          url: 'https://opencollective.com/',
          // you can specify a custom icon for the link if you don't like the
          // default one. use any icon from https://forkawesome.github.io/Fork-Awesome/icons/
          icon: "eur",
        }
      ]
      media: [
        {
          type: 'image',
          // You can reference any file from the public/ directory here
          url: '/assets/guides/backer/screenshot1.png',
          caption: 'A screenshot to help the reader',
        },
        // You can add as many media as you need
      ]
    },
    // You can add as many step as you need, they will be displayed in order.
  ]
}
```

Once you have at least one step for a task, the "Get started" button
will be displayed next to the task on the homepage.

## Translations

We want our guides to be translatable in various language. To achieve that,
we need to mark strings that need to be translated. This include:

- Task name and summary
- Steps title and content
- Media caption
- Link text

To achieve that, we use the `$pgettext` function, as shown below:

```javascript
{
  // we want the task name and summary to be translated
  // The first argument for the function "task name", is a the context
  // for the translation. It's helpful for translators, to know how a given
  // text is used in the interface
  // The second argument, "Donate to the project", is what will be translated
  // and displayed in the interface
  name: v.$pgettext("Task name", "Donate to the project"),
  summary: v.$pgettext("Task summary", "Donations help us keep the project running!"),
  // other attributes don't need translation
  icon: "eur",
  duration: 5,
  slug: 'donate-to-the-project',
}
```

## Project setup
```
# Dependencies for internationalization
apt-get install gettext jq
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run tests
```
yarn run test:unit
```

### Lints and fixes files
```
yarn run lint
```

### Extract translations in po files

This will collect strings to translate from the javascript files into PO files
for future translation:

```
./scripts/i18n-extract.sh
```
