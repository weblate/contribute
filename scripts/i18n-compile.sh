#!/bin/bash -eux
locales=$(tail -n +2 src/locales.js | sed -e 's/module.exports = //' | jq '.locales[].code' | xargs echo)
mkdir -p src/translations

for locale in $locales; do
    $(yarn bin)/gettext-compile locales/$locale/LC_MESSAGES/app.po --output src/translations/$locale.json
done
