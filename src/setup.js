import Vue from "vue";
import moment from "moment";
import VueRouter from "vue-router";
import "./components/globals"
import GetTextPlugin from 'vue-gettext'

import locales from '@/locales'

let availableLanguages = (function () {
  let l = {}
  locales.locales.forEach(c => {
    l[c.code] = c.label
  })
  return l
})()
let defaultLanguage = 'en_US'
// if (availableLanguages[store.state.ui.currentLanguage]) {
//   defaultLanguage = store.state.ui.currentLanguage
// }
Vue.use(GetTextPlugin, {
  availableLanguages: availableLanguages,
  defaultLanguage: defaultLanguage,
  languageVmMixin: {
    computed: {
      currentKebabCase: function () {
        return this.current.toLowerCase().replace('_', '-')
      }
    }
  },
  translations: {},
  silent: true
})



Vue.config.productionTip = false;

export function ago(date) {
  const m = moment(date);
  return m.fromNow();
}

Vue.filter("ago", ago);
Vue.use(VueRouter)

export default {}
