export function getTasks (v) {
  return [
    {
      name: v.$pgettext('Task name', "Do some translations of Framasoft's projects"),
      duration: 15,
      icon: "language",
      tags: [ v.$pgettext('Task tag', 'Translation') ],
      summary: v.$pgettext("Task summary", 'Help translate some projects into other languages.'),
      slug: 'translate-framasoft',
      skills: [
        {
          name: v.$pgettext("Skill name", 'Comprehend English'),
          summary: v.$pgettext("Skill summary", "You will often need to understand written English to translate Funkwhale to another language.")
        },
        {
          name: v.$pgettext("Skill name", 'Be fluent in another language'),
          summary: v.$pgettext("Skill summary", "Good, you meet the requirements of translating Funkwhale into that language.")
        },
      ],
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Create your Contributor Account'),
          content: [v.$pgettext("Guide step content", "You'll need an account on the platform we use to translate projects."),],
          links: [
            {
              icon: 'language',
              text: v.$pgettext("Text for link in guide step", 'Create your Weblate account'),
              to: {name: 'guide', params: {slug: 'weblate-account', locale: v.$language.current}},
            }
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Choose the project you want to translate'),
          content: [v.$pgettext("Guide step content", "Every project can be made up of several subprojects, also known as components in Weblate's terminology. Pick your favourite, or \"Contribute\" to translate this website."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'List of translatable projects on Weblate'),
              url: 'https://weblate.framasoft.org/projects/'
            }
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/translate/component-list.png',
              caption: v.$pgettext("Task image caption", 'List of available components and translation progress for each')
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Understand what the project is about'),
          content: [v.$pgettext("Guide step content", "In order to translate appropriately a project, it is better to understand what's it about."),],
        },
        {
          title: v.$pgettext("Guide step title", "Review the eventual translation guidelines"),
          content: [v.$pgettext("Guide step content", "Translator guidelines are often available to ensure consistency across languages and components. Take a moment to read this document if available."),],
        },
        {
          title: v.$pgettext("Guide step title", 'Choose the target language'),
          content: [v.$pgettext("Guide step content", "Each component is translated in many languages. Pick a language your are fluent in."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'List of already available languages'),
              url: 'https://translate.funkwhale.audio/projects/funkwhale/front/'
            }
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/translate/language-list.png',
              caption: v.$pgettext("Task image caption", 'List of available languages with progressing translations')
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Log in on Weblate'),
          content: [v.$pgettext("Guide step content", 'Weblate is where translations happen. Log in with your GitLab account using the "Third party login" section on the right to start translating.'),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Log in on Weblate with GitLab'),
              url: 'https://translate.funkwhale.audio/accounts/login/?next=/projects/funkwhale/front/'
            }
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/translate/login-form.png',
              caption: v.$pgettext("Task image caption", "Weblate's login form with GitLab login on the right")
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Configure your languages'),
          content: [v.$pgettext("Guide step content", "Telling Weblate what languages you understand/translate will make it easier afterwards. You can also take a moment to edit your account profile and settings if needed."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Configure your languages'),
              url: 'https://translate.funkwhale.audio/accounts/profile/'
            }
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/translate/profile-languages.png',
              caption: v.$pgettext("Task image caption", "Pick one or more translated and/or secondary languages in the form")
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Click "Translate"'),
          content: [v.$pgettext("Guide step content", 'Click on the "translate" button in the last column of the languages table to start working.'),],
          media: [
            {
              type: 'image',
              url: '/assets/guides/translate/translate-button.png',
              caption: v.$pgettext("Task image caption", "The translate button is located in the last column")
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Submit your first translation'),
          content: [v.$pgettext("Guide step content", "Weblate will now present you with text in need of translation or correction. Confidently submit a translation, or fill-in your proposal and submit the form. If you're not sure at all, comment or move to the next string."),],
          media: [
            {
              type: 'image',
              url: '/assets/guides/translate/translation-form-complete.png',
              caption: v.$pgettext("Task image caption", "A complete overview of the translation form")
            },
            {
              type: 'image',
              url: '/assets/guides/translate/translation-form-translate-area.png',
              caption: v.$pgettext("Task image caption", "The translate area is where most of your work will happen")
            },
            {
              type: 'image',
              url: '/assets/guides/translate/translation-form-action-bar.png',
              caption: v.$pgettext("Task image caption", "The top bar helps you navigate the translations and see your progress")
            },
            {
              type: 'image',
              url: '/assets/guides/translate/translation-form-check.png',
              caption: v.$pgettext("Task image caption", "The \"Checks\" area tells you what you need to do for a given translation")
            },
            {
              type: 'image',
              url: '/assets/guides/translate/translation-form-menu.png',
              caption: v.$pgettext("Task image caption", "Use the submenu to browse similar translations, see other translators comments, suggestions and past activity")
            },
            {
              type: 'image',
              url: '/assets/guides/translate/translation-form-source.png',
              caption: v.$pgettext("Task image caption", 'The "Source information" section can help you find contextual info about a translation, and see the source-code in which the translation is used')
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Stop here… or continue!'),
          content: [v.$pgettext("Guide step content", "You can repeat the previous step until you run out of time. Each new or improved translation makes the project better."),],
        },
      ]
    },
    {
      name: v.$pgettext('Task name', "Learn to edit OpenStreetMap"),
      icon: "map",
      slug: 'openstreetmap',
      tags: [ v.$pgettext('Task tag', 'OpenStreetMap') ],
      duration: 15,
      summary: v.$pgettext("Task summary", "Add details to the community-based world map"),
      equipments: [
        {
          name: v.$pgettext("Skill name", 'A mouse'),
          summary: v.$pgettext("Skill summary", "Editing the map is easier with a mouse in hand.")
        },
      ],
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Head over to openstreetmap.org'),
          content: [
              v.$pgettext("Guide step content", 'Get used to the interface by try to view in detail your current location, or another location of your choice. You may also search a location by name in the search bar.')
          ],
          links: [
            {
              icon: 'map',
              text: v.$pgettext("Text for link in guide step", 'Access openstreetmap.org'),
              url: 'https://openstreetmap.org'
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Register an account'),
          content: [
            v.$pgettext("Guide step content", "Modifications are only allowed to registered users, so let's click on \"Register\" on the top right of the screen and create an account.")
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Enter edit mode'),
          content: [
            v.$pgettext("Guide step content", "Once registered, let's enter edit mode to make your first modifications."),
            v.$pgettext("Guide step content", "After a few seconds, the edition interface should be shown"),
          ]
        }
      ]
    },
    {
      name: v.$pgettext('Task name', "Report a bug"),
      icon: "bug",
      slug: 'report-a-bug',
      tags: [ v.$pgettext('Task tag', 'Feedback') ],
      duration: 15,
      summary: v.$pgettext("Task summary", "Report typos, display issues, or other unexpected bebaviour, or fix it yourself."),
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Search for similar bugs'),
          content: [
            v.$pgettext("Guide step content", "A quick search can yield similar bug and avoid opening duplicates. Take a minute or two to search our issue tracker for recent bug reports that could match yours, but don't worry if you open a duplicate: a contributor will handle it :)"),
            v.$pgettext("Guide step content", "If you find a similar bug report, you can comment on this one instead of creating a new one."),
            v.$pgettext("Guide step content", "If you are unsure about submitting a bug report or have other general tech questions, you can also start a thread in our Loomio support group, or ask us on our Fediverse account."),
          ],
          links: [
            {
              icon: 'gitlab',
              text: v.$pgettext("Text for link in guide step", 'Browse existing bugs'),
              url: 'https://dev.funkwhale.audio/funkwhale/funkwhale/issues?label_name%5B%5D=Type%3A+Bug',
            },
            {
              icon: 'comment',
              text: v.$pgettext("Text for link in guide step", 'Start a thread on our support forum'),
              url: 'https://governance.funkwhale.audio/g/246YOJ1m/funkwhale-support',
            },
            {
              icon: 'mastodon',
              text: v.$pgettext("Text for link in guide step", 'Get in touch on the Fediverse'),
              url: 'https://mastodon.eliotberriot.com/@funkwhale',
            }
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Create your Contributor Account'),
          content: [v.$pgettext("Guide step content", 'This account is needed to submit your bug report or comments.'),],
          links: [
            {
              icon: 'gitlab',
              text: v.$pgettext("Text for link in guide step", 'Create your GitLab account'),
              to: {name: 'guide', params: {slug: 'gitlab-account', locale: v.$language.current}},
            }
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Write your bug report'),
          content: [
            v.$pgettext("Guide step content", 'Select "Bug" in the issue template dropdown, then fill the title and content for your bug report.'),
            v.$pgettext("Guide step content", 'Try to be as precise and specific as possible: the other contributors that will read your report may not know things that are obvious to you.'),
            v.$pgettext("Guide step content", "We know writing an actionable bug report is hard. Do your best and give as much context as possible, but don't worry: other contributors will reach out to you if they need more details."),
          ],
          recommendations: [
            v.$pgettext("Advice content / recommendations", "Explain what you were trying to do"),
            v.$pgettext("Advice content / recommendations", "Explain what you expected to happen"),
            v.$pgettext("Advice content / recommendations", "Explain what actually happened"),
            v.$pgettext("Advice content / recommendations", "Include logs, error messages, screenshots and information about your system (web browser, operating system…), if applicable"),
            v.$pgettext("Advice content / recommendations", "Include the version number of the Funkwhale server on which the issue happens"),
          ],
          avoid: [
            v.$pgettext("Advice content / avoid", 'Use vague language such as "this is not working"'),
          ],
          links: [
            {
              icon: 'pencil',
              text: v.$pgettext("Text for link in guide step", 'Write your bug report'),
              url: "https://dev.funkwhale.audio/funkwhale/funkwhale/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=",
            }
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/report-a-bug/template.png',
              caption: v.$pgettext("Task image caption", 'Choose "bug" in the issue template drowdown')
            },
            {
              type: 'image',
              url: '/assets/guides/report-a-bug/log.png',
              caption: v.$pgettext("Task image caption", 'Put logs between three ` (backticks)')
            },
            {
              type: 'image',
              url: '/assets/guides/report-a-bug/full-example.png',
              caption: v.$pgettext("Task image caption", "A complete exemple of a bug report")
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Review and submit your report'),
          content: [
            v.$pgettext("Guide step content", 'Read your report one last time, then click on the "Submit" button.'),
            v.$pgettext("Guide step content", 'A contributor will have a look at it within the next days!'),
          ],
        }
      ]
    },
    {
      name: v.$pgettext('Task name', "Review the documentation"),
      icon: "search",
      duration: 15,
      tags: [ v.$pgettext("Task tag", 'Documentation') ],
      slug: 'review-documentation',
      summary: v.$pgettext("Task summary", "Ensure the documentation is accurate, understandable and up-to-date."),
      steps: [
        {
          title: v.$pgettext("Guide step title", "Head over to the documentation and pick a service"),
          content: [
            v.$pgettext("Guide step content", "You can start by picking a service you know well so that you don't need to get to know about the service and it's interface."),
            v.$pgettext("Guide step content", "Otherwise, just pick one at random and use it a bit to understand what it's all about.")
          ],
          links: [
            {
              icon: 'book',
              text: v.$pgettext('Text for link in guide step', 'View the documentation index'),
              url: 'https://docs.framasoft.org/'
            }
          ]
        },
        {
          title: v.$pgettext('Guide step title', 'Proofread the documentation carefully'),
          content: [
              v.$pgettext('Guide step content', 'You can check for the following issues:'),
          ],
          recommendations: [
            v.$pgettext("Advice content / recommendations", "Typos"),
            v.$pgettext("Advice content / recommendations", "Outdated instructions"),
            v.$pgettext("Advice content / recommendations", "Outdated screenshots"),
          ],
        },
        {
          title: v.$pgettext('Guide step title', 'Take note of each issue'),
          content: [
            v.$pgettext('Guide step content', "The nature of the issue and it's position"),
          ],
        },
        {
          title: v.$pgettext('Guide step title', 'Report the issues or fix them yourself'),
          content: [
            v.$pgettext('Guide step content', "Depending on how much time and how much motivation you have left, you can try to fix the issues yourself or report them"),
          ],
          links: [
            {
              icon: 'code',
              text: v.$pgettext('Text for link in guide step', 'Submit patches for the documentation'),
              to: {name: 'guide', params: {slug: 'patch-documentation', locale: v.$language.current}},
            },
            {
              icon: 'bug',
              text: v.$pgettext('Text for link in guide step', 'Report documentation issues'),
              to: 'https://framagit.org/framasoft/docs/issues/',
            }
          ]
        }
      ]
    },
    {
      name: v.$pgettext('Task name', "Submit patches for the documentation"),
      icon: "pencil",
      duration: 30,
      tags: [ v.$pgettext("Task tag", 'Documentation') ],
      slug: 'patch-documentation',
      summary: v.$pgettext("Task summary", "Fix issues and improve the quality of the documentation."),
    },
    {
      name: v.$pgettext('Task name', "Request a feature"),
      icon: "question",
      slug: "request-a-feature",
      tags: [ v.$pgettext('Task tag', 'Feedback') ],
      duration: 30,
      summary: v.$pgettext("Task summary", "Is something missing in a project you use? Write a feature request and spark a discussion about it."),
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Search for similar requests'),
          content: [
            v.$pgettext("Guide step content", 'It\'s possible that the feature you want was already requested by someone else. Take a minute or two to look at our roadmap and at the "Feature Request" category on our forum.'),
            v.$pgettext("Guide step content", "If you find a similar request, it will be more productive to join the existing discussion instead of opening a new one. Don't worry if you open a duplicate: a contributor will handle it :)"),
          ],
          links: [
            {
              icon: 'comment',
              text: v.$pgettext("Text for link in guide step", 'Browse existing requests'),
              url: 'https://governance.funkwhale.audio/tags/1/feature-request?group_key=l7yxBtQT',
            },
            {
              icon: 'map-o',
              text: v.$pgettext("Text for link in guide step", 'Read our roadmap'),
              url: 'https://pad.funkwhale.audio/roadmap',
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Write your request'),
          content: [
            v.$pgettext("Guide step content", "Take some time to write your request. Explain what you want to achieve, why it's not possible to do it currently, how it could improve the project."),
            v.$pgettext("Guide step content", "Give as much context as possible so other readers can understand your needs."),
            v.$pgettext("Guide step content", "It's also extremely important to describe the issue you want to solve instead of discribing a solution to this issue, because different or more efficient solutions you don't know could exist."),
          ],
          recommendations: [
            v.$pgettext("Advice content / recommendations", "Explain what you need"),
            v.$pgettext("Advice content / recommendations", "Share links and references to projects that have a similar feature"),
          ],
          avoid: [
            v.$pgettext("Advice content / avoid", 'Request a specific solution'),
          ],
          links: [
            {
              icon: 'pencil',
              text: v.$pgettext("Text for link in guide step", 'Write your request'),
              url: "https://governance.funkwhale.audio/g/kQgxNq15/funkwhale",
            }
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/request-a-feature/new-thread.png',
              caption: v.$pgettext("Task image caption", 'Start a thread by clicking the "New Thread" button')
            },
            {
              type: 'image',
              url: '/assets/guides/request-a-feature/full-example.png',
              caption: v.$pgettext("Task image caption", "A simple feature request")
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Review and submit your request'),
          content: [
            v.$pgettext("Guide step content", 'Read your request one last time, then click on the "Submit" button.'),
            v.$pgettext("Guide step content", 'Other contributors will soon join the discussion and give you feedback.'),
          ],
        }
      ]
    },
    {
      name: v.$pgettext('Task name', "Support other community members"),
      icon: "wrench",
      slug: "support-community-members",
      tags: [ v.$pgettext('Task tag', 'Support') ],
      duration: 30,
      summary: v.$pgettext("Task summary", "Help other community members troubleshot and solve their issues while installing or using Framasoft services."),
      skills: [
        {
          name: v.$pgettext('Skill name', 'Good interpersonal skills'),
          summary: v.$pgettext('Skill summary', "Sometime you'll need extra patience")
        },
        {
          name: v.$pgettext('Skill name', 'Knowledge of some projects'),
        }
      ],
      steps: [
        {
          title: v.$pgettext("Guide step title", "Register an account on Framacolibri"),
          content: [
              v.$pgettext("Guide step content", 'It will just take a second and is required to post on the Framacolibri forum.'),
          ],
        }
      ]
    },
    {
      name: v.$pgettext('Task name', "Write about a project you like"),
      icon: "pencil",
      duration: 60,
      tags: [ v.$pgettext('Task tag', 'Communication') ],
      summary: v.$pgettext("Task summary", "Do you own a blog or are you the media? Indemnify yourself by writing about a project you like to help it attain eyeballs."),
    },
    {
      name: v.$pgettext('Task name', "Solve a small issue"),
      icon: "file-code-o",
      tags: [ v.$pgettext('Task tag', 'Code') ],
      duration: 60,
      summary: v.$pgettext("Task summary", "Improve the project by fixing a reported bug or implementing a requested enhancement."),
    },
    {
      name: v.$pgettext('Task name', "Audit a project security"),
      icon: "user-secret",
      tags: [ v.$pgettext('Task tag', 'Code') ],
      duration: 120,
      summary: v.$pgettext("Task summary", "Investigate and report possible security issues, and help solve them before users are affected. Please don't do this on Framasoft's infrastructure but through installing the projects yourself."),
    },
    {
      name: v.$pgettext('Task name', 'Learn how to contribute to Wikipedia'),
      icon: 'wikipedia-w',
      tags: [ v.$pgettext('Task tag', 'Wikipedia') ],
      duration: 15,
      summary: v.$pgettext("Task summary", 'Wikipedia is useful to all of us, why not contribute to it ?')
    },
    {
      name: v.$pgettext('Task name', 'Create and improve Wikipedia articles about Women'),
      icon: 'venus',
      tags: [ v.$pgettext('Task tag', 'Wikipedia') ],
      duration: 60,
      summary: v.$pgettext("Task summary", "Women are under-represented on Wikipedia because of the Gender bias. Let's change that!")
    },
    // hidden tasks (on home) that can be included in other guides
    {
      name: v.$pgettext('Task name', "Create a Framagit account"),
      icon: "gitlab",
      duration: 5,
      unlisted: true,
      slug: 'gitlab-account',
      summary: v.$pgettext("Task summary", 'Create your GitLab account on framagit.org to start working on various projects.'),
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Visit framagit.org'),
          content: [v.$pgettext("Guide step content", 'The Funkwhale GitLab instance is hosted at dev.funkwhale.audio. You have to sign-up here, using your e-mail account or by way of a third-party token provider.'),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Sign up on framagit.org'),
              url: 'https://framagit.org/users/sign_in',
            },
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/gitlab-account/signup-form.png',
              caption: v.$pgettext("Task image caption", 'Sign-up form')
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Verify your e-mail address'),
          content: [v.$pgettext("Guide step content", "Shortly after signup, you should receive a confirmation e-mail containing an activation link. Click it to activate your account. Request another one if you did not receive the e-mail within a few minutes."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Request another activation e-mail'),
              url: 'https://dev.funkwhale.audio/users/almost_there',
            }
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Log in'),
          content: [v.$pgettext("Guide step content", "Log in using your new account to ensure everything works properly."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Login form'),
              url: 'https://dev.funkwhale.audio/users/sign_in',
            }
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/gitlab-account/login-form.png',
              caption: v.$pgettext("Task image caption", 'Login form')
            },
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Customize your profile'),
          optional: true,
          content: [v.$pgettext("Guide step content", "Take a few minutes to customize your profile and upload your avatar. This will help other community members know who you are :)"),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Profile form'),
              url: 'https://dev.funkwhale.audio/profile',
            }
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/gitlab-account/user-dropdown.png',
              caption: v.$pgettext("Task image caption", 'Settings dropdown to access the profile form')
            },
            {
              type: 'image',
              url: '/assets/guides/gitlab-account/profile-form.png',
              caption: v.$pgettext("Task image caption", 'The profile form')
            },
          ],
        }
      ]
    },
    {
      name: v.$pgettext('Task name', "Create a Weblate account"),
      icon: "language",
      duration: 5,
      unlisted: true,
      slug: 'weblate-account',
      summary: v.$pgettext("Task summary", 'Create your Weblate account on weblate.framasoft.org to start translating various projects.'),
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Visit weblate.framasoft.org'),
          content: [
              v.$pgettext("Guide step content", 'The Framasoft Weblate instance is hosted at weblate.framasoft.org. You have to sign-up here by using your e-mail account.'),
            v.$pgettext("Guide step content", 'Note : This email may be publicly accessible in the project history'),
          ],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Sign up on weblate.framasoft.org'),
              url: 'https://weblate.framasoft.org/accounts/register/',
            },
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/weblate-account/signup-form.png',
              caption: v.$pgettext("Task image caption", 'Sign-up form')
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Verify your e-mail address'),
          content: [v.$pgettext("Guide step content", "Shortly after signup, you should receive a confirmation e-mail containing an activation link. Click it to activate your account. Request another one if you did not receive the e-mail within a few minutes."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Request another activation e-mail'),
              url: 'https://dev.funkwhale.audio/users/almost_there',
            }
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Log in'),
          content: [v.$pgettext("Guide step content", "Log in using your new account to ensure everything works properly."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Login form'),
              url: 'https://dev.funkwhale.audio/users/sign_in',
            }
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/gitlab-account/login-form.png',
              caption: v.$pgettext("Task image caption", 'Login form')
            },
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Customize your profile'),
          optional: true,
          content: [v.$pgettext("Guide step content", "Take a few minutes to customize your profile and upload your avatar. This will help other community members know who you are :)"),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Profile form'),
              url: 'https://dev.funkwhale.audio/profile',
            }
          ],
          media: [
            {
              type: 'image',
              url: '/assets/guides/gitlab-account/user-dropdown.png',
              caption: v.$pgettext("Task image caption", 'Settings dropdown to access the profile form')
            },
            {
              type: 'image',
              url: '/assets/guides/gitlab-account/profile-form.png',
              caption: v.$pgettext("Task image caption", 'The profile form')
            },
          ],
        }
      ]
    },
  ]
}
