import { getTasks } from '../data'

export default {
  computed: {
    tasks () {
      return getTasks(this)
    },
    tasksBySlug () {
      let t = {}
      this.tasks.forEach(e => {
        t[e.slug] = e
      });
      return t
    }
  }
}
